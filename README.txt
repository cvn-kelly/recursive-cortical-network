
Install: GNU Octave, SourceTree

In Octave:
--Install Packages--
pkg install -forge io
pkg install -forge statistics
pkg install -forge image

--Load Packages--
pkg load statistics
pkg load image


In 'main.m'
im = imread("IMAGE-FILE-NAME");

Program detects 16 line orientations
Press enter to run each direction
Processed image appears in new window