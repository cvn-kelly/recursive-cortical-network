function x = clip (x, range = [0, 1])

  if (nargin < 1 || nargin > 2)
    print_usage;
  else
    if (numel (range) == 2)
      ## do nothing, it's good
    elseif (numel (range) == 1)
      range = [0, range];
    else
      print_usage;
    endif
  endif

  x (x > range (2)) = range (2);
  x (x < range (1)) = range (1);

endfunction