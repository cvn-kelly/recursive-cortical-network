
#im = imread("lena512.bmp");
#im = imread("lena128.png");
im = imread("lena_std.tif");
rs = size(im, 1);
cols = size(im, 2);
steps = 16;
clock()
out = preprocess(im, steps, 7);
clock()
#normalized_out = out - min(min(out));
#normalized_out = normalized_out / max(max(normalized_out));
for i= 1:steps
  imshow(reshape(out(i, :), rs, cols));
  printf("Step: %d\n", i);
  input("Press enter to continue");
endfor
imshow(reshape(max(out), rs, cols));
