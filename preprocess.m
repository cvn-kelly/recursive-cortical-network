function [processed, directions] = preprocess(im, steps = 16, kernel_size = 8)

  rs = linspace(0, 2 * pi, steps + 1);
  rs = rs(1:steps);
  height = size(im, 1);
  width = size(im, 2);
  edge_filters = [];
  filtered_images = eye(steps, width * height);
  [X, Y] = meshgrid (linspace (-3, 3, kernel_size));
  XY = [X(:), Y(:)];
  sig = [1, 0; 0, 1];
  gray = uint8(mean(im, 3));

  for t = 1:steps
    r = rs(t);
    mu = [cos(r), sin(r)];
    Z = mvnpdf(XY, mu, sig) - mvnpdf(XY, mu .* -1, sig);
    edge_filters(t, :) = Z;
    
    filter = reshape(Z, kernel_size, kernel_size);
    #imshow(filter);
    #input("filter");
    filtered_image = imfilter(im, filter, "replicate");

    if size(im, 3) > 1
      filtered_image(:, :, 4) = imfilter(gray, filter, "replicate");
      filtered_image = max(filtered_image, [], 3);
      else
    endif
    #imshow(uint8(filtered_image));
    #input("filtered");
    degrees = r * 180 / pi;
    perp_degrees = mod(degrees + 90, 360);
    neighbors = strel("line", kernel_size, perp_degrees);
    dilated = imdilate(filtered_image, neighbors);
    #imshow(uint8(dilated));
    #input("dilated");
    local_max = dilated == filtered_image;
    #imshow(local_max);
    #input("local_max");
    localized = local_max .* filtered_image;

    #imshow(uint8(localized));
    #input("localized");
      
    filtered_images(t, :) = localized(:);
  endfor
  #correlated = max(filtered_images, [], 1) / 255;

  [filt_max, directions] = max(filtered_images);
  for t = 1:steps
    is_max = filtered_images(t, :) == filt_max;
    filtered_images(t, :) = filtered_images(t, :) .* is_max;
  endfor

  tau = 30;
  clipped = double(clip(filtered_images / tau));
  s = 1 / (1 + e);
  smoothed = s .+ (1 - 2 * s) .* clipped;
  
  loglike = log(smoothed) - log( -1 .* smoothed + 1);
  gamma = .9;
  gamma_loglike = loglike * gamma;
  wrapped = [ gamma_loglike(steps, :) ;  gamma_loglike; gamma_loglike(1, :)];
  vert_neighbors = strel("line", 3, 90);
  dilated_wrapped = imdilate(wrapped, vert_neighbors);
  use_pool = dilated_wrapped(2:steps + 1,:) > loglike;
  use_loglike = ones(size(loglike)) - use_pool;
  processed = loglike .* use_loglike + dilated_wrapped(2:steps+1,:) .* use_pool;

endfunction